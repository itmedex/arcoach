using UnityEngine;
using System;
using System.Collections;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

/*
 * DESCRIPTION
 * 
 * This script receives the information about positions and orientations from the Kinect sensor.
 * 
 * It is important not to change the names of all the variables. This is important because the information received
 * is dependent of the names on the script MainWindow of MicrosoftVisualStudio. Furthermore, the positions of some 
 * GameObjects are also dependent of these names.
 * 
 * Attach this script to the KinectData
 */


public class UDPReceive : MonoBehaviour
{
	static System.Globalization.NumberFormatInfo ni = null;

	//received data

	//Head
	public Vector3 headPos = new Vector3 (0, 0, 0);

	public Vector3 neckPos = new Vector3 (0, 0, 0);

	//Spine
	public Vector3 spineShoulderPos = new Vector3 (0, 0, 0);
	public Vector3 spineMidPos = new Vector3 (0, 0, 0);
	public Vector3 spineBasePos = new Vector3 (0, 0, 0);

	//Left
	public Vector3 leftShoulderPos = new Vector3(0, 0, 0);
	public Vector3 leftElbowPos = new Vector3(0, 0, 0);
	public Vector3 leftWristPos = new Vector3(0, 0, 0);
	public Vector3 leftHandPos = new Vector3 (0, 0, 0);
	public Vector3 leftThumbPos = new Vector3(0, 0, 0);
	public Vector3 leftHandTipPos = new Vector3(0, 0, 0);

	public Vector3 leftHipPos = new Vector3 (0, 0, 0);
	public Vector3 leftKneePos = new Vector3(0, 0, 0);
	public Vector3 leftAnklePos = new Vector3(0, 0, 0);
	public Vector3 leftFootPos = new Vector3(0, 0, 0);

	//Right
	public Vector3 rightShoulderPos = new Vector3(0, 0, 0);
	public Vector3 rightElbowPos = new Vector3(0, 0, 0);
	public Vector3 rightWristPos = new Vector3(0, 0, 0);
	public Vector3 rightHandPos = new Vector3 (0, 0, 0);
    public Vector3 rightThumbPos = new Vector3(0, 0, 0);
	public Vector3 rightHandTipPos = new Vector3(0, 0, 0);

	public Vector3 rightHipPos = new Vector3 (0, 0, 0);
	public Vector3 rightKneePos = new Vector3(0, 0, 0);
	public Vector3 rightAnklePos = new Vector3(0, 0, 0);
	public Vector3 rightFootPos = new Vector3(0, 0, 0);

	
    //ORIENTATION
	public Vector4 headOri = new Vector4 (0, 0, 0, 0);
	public Vector4 neckOri = new Vector4 (0, 0, 0, 0);
	
	//Spine
	public Vector4 spineShoulderOri = new Vector4 (0, 0, 0, 0);
	public Vector4 spineMidOri = new Vector4 (0, 0, 0, 0);
	public Vector4 spineBaseOri = new Vector4 (0, 0, 0, 0);

	//Left
	public Vector4 leftShoulderOri = new Vector4 (0, 0, 0, 0);
	public Vector4 leftElbowOri = new Vector4 (0, 0, 0, 0);
	public Vector4 leftWristOri = new Vector4 (0, 0, 0, 0);
	public Vector4 leftHandOri = new Vector4 (0, 0, 0, 0);
	public Vector4 leftThumbOri = new Vector4 (0, 0, 0, 0);
	public Vector4 leftHandTipOri = new Vector4 (0, 0, 0, 0);
	
	public Vector4 leftHipOri = new Vector4 (0, 0, 0, 0);
	public Vector4 leftKneeOri = new Vector4 (0, 0, 0, 0);
	public Vector4 leftAnkleOri = new Vector4 (0, 0, 0, 0);
	public Vector4 leftFootOri = new Vector4 (0, 0, 0, 0);
	
	//Right
	public Vector4 rightShoulderOri = new Vector4 (0, 0, 0, 0);
	public Vector4 rightElbowOri = new Vector4 (0, 0, 0, 0);
	public Vector4 rightWristOri = new Vector4 (0, 0, 0, 0);
	public Vector4 rightHandOri = new Vector4 (0, 0, 0, 0);
	public Vector4 rightThumbOri = new Vector4 (0, 0, 0, 0);
	public Vector4 rightHandTipOri = new Vector4 (0, 0, 0, 0);

	public Vector4 rightHipOri = new Vector4 (0, 0, 0, 0);
	public Vector4 rightKneeOri = new Vector4(0, 0, 0, 0);
	public Vector4 rightAnkleOri = new Vector4(0, 0, 0, 0);
	public Vector4 rightFootOri = new Vector4(0, 0, 0, 0);

    //Hand States
    public string lHandState;
    public string rHandState;

    public bool receivingMsg = false;

	// receiving Thread
	Thread receiveThread;
	
	// udpclient object
	System.Net.Sockets.UdpClient client;
	
	// public
	//public string IP = "127.0.0.1"; default local 
	public int port; // define > init

	// start from shell
	private static void Main()
	{
		UDPReceive receiveObj = new UDPReceive();
		receiveObj.init();
		
		string text = "";
		do {
			text = Console.ReadLine ();
		} while(!text.Equals("exit"));
	}

	// start from unity3d
	public void Start()
	{
		System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.InstalledUICulture;
		ni = (System.Globalization.NumberFormatInfo)ci.NumberFormat.Clone();

		init();
	}
	
	// init
	private void init()
	{
		receiveThread = new Thread(
			new ThreadStart(ReceiveData));
		receiveThread.IsBackground = true;
		receiveThread.Start();
	}
	
	// receive thread
	private void ReceiveData()
	{	
		client = new System.Net.Sockets.UdpClient (port);
		while (true) {	
			try {
				IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);
				byte[] data = client.Receive(ref anyIP);

				string text = Encoding.UTF8.GetString(data);
				receivingMsg = true;

				dataParser(text);			
			} catch (Exception err) {
				print(err.ToString ());
			}
		}
	}

	void OnDisable ()
	{ 
		if (receiveThread != null) 
			receiveThread.Abort (); 
		
		client.Close (); 
	} 

	public void dataParser (string text)
	{
		char[] delimiterChars = {' ' , '\n'};
		
		string[] words = text.Split (delimiterChars);

		for (int i = 0; i < words.Length - 1;) {
			float number1 = 0;
			float number2 = 0;
			float number3 = 0;
			float number4 = 0;
			//int number5 = 0;
			int aux = i + 1;
		
			switch (words [i]) {
			    case "headPos":
			    case "neckPos":
			    case "leftHandPos":
			    case "leftWristPos":
                case "leftElbowPos":
                case "leftShoulderPos":
                case "rightHandPos":
                case "rightWristPos":
                case "rightElbowPos":
                case "rightShoulderPos":
                case "leftThumbPos":
                case "rightThumbPos":
                case "leftHandTipPos":
                case "rightHandTipPos":
			    case "spineShoulderPos":
			    case "spineMidPos":
			    case "spineBasePos":
			    case "rightHipPos":
			    case "rightKneePos":
			    case "rightAnklePos":
			    case "rightFootPos":
			    case "leftHipPos":
			    case "leftKneePos":
			    case "leftAnklePos":
			    case "leftFootPos":
				    number1 = float.Parse (words [i + 1], ni);
				    number2 = float.Parse (words [i + 2], ni);
				    number3 = float.Parse (words [i + 3], ni);
				    aux = i + 4;
				    break;

			    case "headOri":
			    case "neckOri":
			    case "leftHandOri":
			    case "leftWristOri":
			    case "leftElbowOri":
			    case "leftShoulderOri":
			    case "rightHandOri":
			    case "rightWristOri":
			    case "rightElbowOri":
			    case "rightShoulderOri":
			    case "leftThumbOri":
			    case "rightThumbOri":
			    case "leftHandTipOri":
			    case "rightHandTipOri":
			    case "spineShoulderOri":
			    case "spineMidOri":
			    case "spineBaseOri":
			    case "rightHipOri":
			    case "rightKneeOri":
			    case "rightAnkleOri":
			    case "rightFootOri":
			    case "leftHipOri":
			    case "leftKneeOri":
			    case "leftAnkleOri":
			    case "leftFootOri":
                    number1 = float.Parse (words [i + 1], ni);
				    number2 = float.Parse (words [i + 2], ni);
				    number3 = float.Parse (words [i + 3], ni);
				    number4 = float.Parse (words [i + 4], ni);
				    aux = i + 5;
				    break;

                case "lHandState":
                        lHandState = words[i + 1];
                        aux = i + 2;
                        break;
                case "rHandState":
                    rHandState = words[i + 1];
                    aux = i + 2;
                    break;
			    default:
				    break;
			}

			switch (words [i]) {
			    case "headPos":
				    headPos = new Vector3 (number1, number2, -number3);
				    break;
			    case "neckPos":
				    neckPos = new Vector3 (number1, number2, -number3);
				    break;
			    case "leftHandPos":
				    leftHandPos = new Vector3 (number1, number2, -number3);
				    break;
                case "leftWristPos":
                    leftWristPos = new Vector3(number1, number2, -number3);
                    break;
                case "leftElbowPos":
                    leftElbowPos = new Vector3(number1, number2, -number3);
                    break;
                case "leftShoulderPos":
                    leftShoulderPos = new Vector3(number1, number2, -number3);
                    break;
			    case "rightHandPos":
				    rightHandPos = new Vector3 (number1, number2, -number3);
				    break;
                case "rightWristPos":
                    rightWristPos = new Vector3(number1, number2, -number3);
                    break;
                case "rightElbowPos":
                    rightElbowPos = new Vector3(number1, number2, -number3);
                    break;
                case "rightShoulderPos":
                    rightShoulderPos = new Vector3(number1, number2, -number3);
                    break;
			    case "leftThumbPos":
                    leftThumbPos = new Vector3(number1, number2, -number3);
                    break;
			    case "rightThumbPos":
                    rightThumbPos = new Vector3(number1, number2, -number3);
                    break;
			    case "leftHandTipPos":
                    leftHandTipPos = new Vector3(number1, number2, -number3);
                    break;
			    case "rightHandTipPos":
                    rightHandTipPos = new Vector3(number1, number2, -number3);
                    break;
			    case "spineShoulderPos":
				    spineShoulderPos = new Vector3(number1, number2, -number3);
				    break;
			    case "spineMidPos":
				    spineMidPos = new Vector3(number1, number2, -number3);
				    break;
			    case "spineBasePos":
				    spineBasePos = new Vector3(number1, number2, -number3);
				    break;
			    case "rightHipPos":
				    rightHipPos = new Vector3(number1, number2, -number3);
				    break;
			    case "rightKneePos":
				    rightKneePos = new Vector3(number1, number2, -number3);
				    break;
			    case "rightAnklePos":
				    rightAnklePos = new Vector3(number1, number2, -number3);
				    break;
			    case "rightFootPos":
				    rightFootPos = new Vector3(number1, number2, -number3);
				    break;
			    case "leftHipPos":
				    leftHipPos = new Vector3(number1, number2, -number3);
				    break;
			    case "leftKneePos":
				    leftKneePos = new Vector3(number1, number2, -number3);
				    break;
			    case "leftAnklePos":
				    leftAnklePos = new Vector3(number1, number2, -number3);
				    break;
			    case "leftFootPos":
				    leftFootPos = new Vector3(number1, number2, -number3);
				    break;

			    // ORIENTATION
			    case "headOri":
				    headOri = new Vector4 (number1, number2, number3, number4);
				    break;			
			    case "neckOri":
				    neckOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "leftHandOri":
				    leftHandOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "leftWristOri":
				    leftWristOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "leftElbowOri":
				    leftElbowOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "leftShoulderOri":
				    leftShoulderOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "rightHandOri":
				    rightHandOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "rightWristOri":
				    rightWristOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "rightElbowOri":
				    rightElbowOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "rightShoulderOri":
				    rightShoulderOri = new Vector4 (number1, number2, number3, number4);
				    break;				
			    case "leftThumbOri":
				    leftThumbOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "rightThumbOri":
				    rightThumbOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "leftHandTipOri":
				    leftHandTipOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "rightHandTipOri":
				    rightHandTipOri = new Vector4 (number1, number2, number3, number4);
				    break;				
			    case "spineShoulderOri":
				    spineShoulderOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "spineMidOri":
				    spineMidOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "spineBaseOri":
				    spineBaseOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "rightHipOri":
				    rightHipOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "rightKneeOri":
				    rightKneeOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "rightAnkleOri":
				    rightAnkleOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "rightFootOri":
				    rightFootOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "leftHipOri":
				    leftHipOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "leftKneeOri":
				    leftKneeOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "leftAnkleOri":
				    leftAnkleOri = new Vector4 (number1, number2, number3, number4);
				    break;
			    case "leftFootOri":
				    leftFootOri = new Vector4 (number1, number2, number3, number4);
				    break;
                default:
			    break;
			}
			
			i = aux;
		}
	}

}
