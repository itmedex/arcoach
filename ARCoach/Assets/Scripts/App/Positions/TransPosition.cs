﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script gives the position for the torso.
 * 
 * It receives the spineMid joint whose position will be the same for the torso.
 * 
 * Attach this script to all torsos in Body Parts (Orthogonalization)
 */

public class TransPosition : MonoBehaviour {

	public GameObject joint;
	
	private Transform trans;
	
	private Transform jointtrans;
	
	void Start () 
	{
		trans = GetComponent<Transform> ();
	}
	
	void Update ()
	{
		jointtrans = joint.transform;
		trans.position = jointtrans.position; 	
	}
}
