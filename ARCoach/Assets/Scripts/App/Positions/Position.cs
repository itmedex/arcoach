﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script calculates the position of all the components (except for torso) of the main body.
 * 
 * It calculates the position by taking the positions of two joints, divide the distance between then and 
 * add the value to one joint. For example, for the right arm it subtracts the position between the rightShoulder 
 * and the rightElbow and, after dividing by two, the position of the RightArm will be the sum of that value 
 * with the rightElbow's position.
 * 
 * It needs to receive the two joints and must be attached to all body parts in Body Parts (Orthogonalization) except 
 * for torso
 */

public class Position : MonoBehaviour {

	public Transform origin;
	public Transform destination;

	private Transform trans;
	private Vector3 diff;

	void Start () 
	{
		trans = GetComponent<Transform> ();
		trans.position = (origin.position - destination.position) / 2;
	}
	

	void Update () 
	{
		diff = (origin.position - destination.position) / 2;
		
		trans.position = destination.position + diff;

	}

}
