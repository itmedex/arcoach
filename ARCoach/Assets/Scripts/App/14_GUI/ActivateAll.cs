﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script activates the GameObject this script is attached to.
 * 
 * It is applied to the main GameObjects of each orthogonalization.
 */

public class ActivateAll : MonoBehaviour {

	public void On()
	{
		gameObject.SetActive (true);
	}
}
