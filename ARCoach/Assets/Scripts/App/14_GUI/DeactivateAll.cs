﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script deactivates the GameObject this script is attached to.
 * 
 * It is applied to the main GameObjects of each orthogonalization.
 */

public class DeactivateAll : MonoBehaviour {

	public void Off()
	{
		gameObject.SetActive (false);
	}
}
