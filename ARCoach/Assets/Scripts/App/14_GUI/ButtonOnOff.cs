﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script activates the GameObject if the GameObject is not activated and vice-versa.
 * 
 * It is applied to the MainCamera and to the main GameObjects of each orthogonalization.
 */

public class ButtonOnOff : MonoBehaviour {

	public void Switch() 
	{
		if (gameObject.activeSelf == true) 
		{
			gameObject.SetActive (false);
		} else 
		{
			gameObject.SetActive (true);
		}
	}

}
