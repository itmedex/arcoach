﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 * DESCRIPTION
 * 
 * This script sets the color of white.
 * 
 * Attach this to the buttons of the Canvas of MainCamera to change the color to white when the pannel
 * is changed.
 */

public class SetWhiteColor : MonoBehaviour {

	Color buttonImageColor;

	public void WhiteColor()
	{
		buttonImageColor = new Color(1, 1, 1, 1);
		gameObject.GetComponent<Image> ().color = buttonImageColor;
	}
}
