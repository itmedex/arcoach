﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 * DESCRIPTION
 * 
 * This script sets the color of red if the color fo the GameObject is white and vice-versa.
 * 
 * Attach this to all the buttons excepted the ChangeView buttons.
 */

public class ButtonColor : MonoBehaviour {

	Color buttonImageColor;

	Color gameObjectcolor;

	public void RedColor(){

		gameObjectcolor = gameObject.GetComponent<Image> ().color;

		if (gameObjectcolor == Color.white) {

			buttonImageColor = new Color(1, 0, 0, 1);
			gameObject.GetComponent<Image> ().color = buttonImageColor;

		} else if (gameObjectcolor == Color.red) {

			buttonImageColor = new Color(1, 1, 1, 1);
			gameObject.GetComponent<Image> ().color = buttonImageColor;

		}

	}


}
