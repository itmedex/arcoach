﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script provides the orientation for the body parts by applying an orthogonalization
 * technique called HouseHolder.
 * 
 * This method only resort to two points instead of three points used in the 3Points method.
 * The logic to convert the vectors to quaternions is the same as the previous method.
 * 
 * Attach this script to all body parts in Body Parts (HouseHolder). Then for each part insert 
 * the two joints more adequate for the movement. 
 */

public class HouseHolder : MonoBehaviour {

	public GameObject firstJoint;
	public GameObject secondJoint;

	private Transform firstJointTrans;
	private Transform secondJointTrans;

	private Vector3 n;
	private float nNorm;

	private Vector3 h;
	private float hNorm;

	private Vector3 h1;
	private Vector3 t;
	private Vector3 b;

	private Matrix4x4 m;
	private Transform trans;

	void Start () 
	{
		trans = GetComponent<Transform> ();
	}
	
	void Update () 
	{
		firstJointTrans = firstJoint.transform;
		secondJointTrans = secondJoint.transform;

		n = firstJointTrans.position - secondJointTrans.position;
		nNorm = Mathf.Sqrt (Mathf.Pow (n.x, 2) + Mathf.Pow (n.y, 2) + Mathf.Pow (n.z, 2));

		h = new Vector3 (Mathf.Max (n.x - nNorm, n.x + nNorm), n.y, n.z);
		hNorm = Mathf.Sqrt (Mathf.Pow (h.x, 2) + Mathf.Pow (h.y, 2) + Mathf.Pow (h.z, 2));

		h1 = new Vector3 (1 - 2 * Mathf.Pow (h.x, 2) / Mathf.Pow (hNorm, 2), -2 * h.x * h.y / Mathf.Pow (hNorm, 2), -2 * h.x * h.z / Mathf.Pow (hNorm, 2));
		t = new Vector3 (-2 * h.x * h.y / Mathf.Pow (hNorm, 2), 1 - 2 * Mathf.Pow (h.y, 2) / Mathf.Pow (hNorm, 2), -2 * h.y * h.z / Mathf.Pow (hNorm, 2));
		b = new Vector3 (-2 * h.x * h.z / Mathf.Pow (hNorm, 2), -2 * h.y * h.z / Mathf.Pow (hNorm, 2), 1 - 2 * Mathf.Pow (h.z, 2) / Mathf.Pow (hNorm, 2));  

		m.SetColumn (0, -t);
		m.SetColumn (1, n);
		m.SetColumn (2, b);

		trans.rotation = QuaternionFromMatrix (m);
	}

	public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
	{ 
		return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
	} 
}
