﻿using UnityEngine;
using System;
using System.Collections;
using System.Linq;

/*
 * DESCRIPTION
 * 
 * This script provides the orientation for the body parts by applying an orthogonalization
 * technique called Projection Matrix.
 * 
 * This method only resort to two points instead of three points used in the 3Points method.
 * The logic to convert the vectors to quaternions is the same as the previous method.
 * 
 * Attach this script to all body parts in Body Parts (Projection Matrix). Then for each part insert 
 * the two joints more adequate for the movement. 
 */

public class ProjectionMatrix : MonoBehaviour {

	public GameObject firstJoint;
	public GameObject secondJoint;
	
	private Transform firstJointTrans;
	private Transform secondJointTrans;

	private Vector3 n;

	private Vector3 n1;
	private Vector3 n2;
	private Vector3 n3;

	private Vector3 normn1;
	private Vector3 normn2;
	private Vector3 normn3;

	private float theta1;
	private float theta2;
	private float theta3;
	private float[] angles = new float[3];
	private int index;

	private float maximum;
	private int posmax;

	private Vector3 u;
	private float[,] R;

	private float[,] normnMatrix;
	private float[,] M;

	private float[,] n1array;
	private float[,] n2array;
	private float[,] n3array;

	private float[,] tfloat = new float[3,1];
	private float[,] bfloat = new float[3,1];

	private Vector3 t;
	private Vector3 b;
	private int aux;

	private Vector3 normn;
	private Vector3 normt;
	private Vector3 normb;

	private Matrix4x4 m;
	private Matrix4x4 m1;
	private Transform trans;

	void Start () {
		trans = GetComponent<Transform> ();
		}
	
	void Update () {

		firstJointTrans = firstJoint.transform;
		secondJointTrans = secondJoint.transform;
		
		n = firstJointTrans.position - secondJointTrans.position;
		n = Vector3.Normalize (n);

		/*
		n1 = new Vector3 (Mathf.Pow (n.x, 2), 
		                  n.x * n.y,
		                  n.x * n.z);

		n2 = new Vector3 (n.x * n.y, 
		                  Mathf.Pow (n.y, 2),
		                  n.y * n.z);

		n3 = new Vector3 (n.x * n.z, 
		                  n.y * n.z,
		                  Mathf.Pow (n.z, 2));


		*/

		n1 = new Vector3 (1, 0, 0);
		n2 = new Vector3 (0, 1, 0);
		n3 = new Vector3 (0, 0, 1);

		normn1 = Vector3.Normalize (n1);
		normn2 = Vector3.Normalize (n2);
		normn3 = Vector3.Normalize (n3);

	
		theta1 = Mathf.Acos (n1.x * n.x + n1.x * n.y + n1.z * n.z);
		theta2 = Mathf.Acos (n2.x * n.x + n2.x * n.y + n2.z * n.z);
		theta3 = Mathf.Acos (n3.x * n.x + n3.x * n.y + n3.z * n.z);

		angles[0] = theta1;
		angles[1] = theta2;
		angles[2] = theta3;


		maximum = angles.Max ();
		posmax = Array.IndexOf (angles, maximum);
		angles [posmax] = float.MinValue;
		index = Array.IndexOf (angles, angles.Max());



		n1array = new float[3, 1] {{normn1.x}, {normn1.y}, {normn1.z}};
		n2array = new float[3, 1] {{normn2.x}, {normn2.y}, {normn2.z}};
		n3array = new float[3, 1] {{normn3.x}, {normn3.y}, {normn3.z}};

		if (index == 0) {

			u = Vector3.Cross (normn1, n);
			R = Rodrigues (u, theta1);
			tfloat = MultiplyMatrix (R, n2array);
			bfloat = MultiplyMatrix (R, n3array);

		} else if (index == 1) {

			u = Vector3.Cross (normn2, n);
			R = Rodrigues (u, theta2);
			tfloat = MultiplyMatrix (R, n1array);
			bfloat = MultiplyMatrix (R, n3array);

		} else if (index == 2) {

			u = Vector3.Cross (normn3, n);
			R = Rodrigues (u, theta3);
			tfloat = MultiplyMatrix (R, n1array);
			bfloat = MultiplyMatrix (R, n2array);
		}

		t.x = tfloat [0, 0];
		t.y = tfloat [1, 0];
		t.z = tfloat [2, 0];

		b.x = bfloat [0, 0];
		b.y = bfloat [1, 0];
		b.z = bfloat [2, 0];

		m1.SetColumn (0, -t);
		m1.SetColumn (1, n);
		m1.SetColumn (2, b);

		trans.rotation = QuaternionFromMatrix (m1);

	}
	
	public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
	{ 
		return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
	} 


	private int PositionAngle (float[] anglevec)
	{

		float max = float.MinValue;
		float secondmax = float.MinValue;

		foreach (int i in anglevec) 
		{			
			if (i > max)
			{
				secondmax = max;
				max = i;
			} else if (i > secondmax)
			{
				secondmax = i;
			}
		}

		int pos = Array.IndexOf (anglevec, secondmax);

		if (pos == -1) {
			pos = 0;
		}

		return pos;
	}

	private float[,] Rodrigues (Vector3 k, float angle)
	{

		float[,] K = new float[3, 3] {{0, -k.z, k.y}, {k.z, 0, -k.x}, {-k.y, k.x, 0}};
		float[,] I = new float[3, 3] {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
		float[,] K2 = MultiplyMatrix (K, K);

		float[,] R = new float[3,3];

		for (int i=0; i <= 2; i++)
		{
			for (int j=0; j <= 2; j++)
			{
				R[i,j] = I[i,j] + Mathf.Sin (angle) * K[i,j] + (1 - Mathf.Cos (angle)) * K2[i,j];
			}
		}

		return R;
	}

	private float[,] MultiplyMatrix(float[,] A, float[,] B)
	{
		int rA = A.GetLength(0);
		int cA = A.GetLength(1);
		int rB = B.GetLength(0);
		int cB = B.GetLength(1);
		float temp = 0.0f;
		float[,] res = new float[rA, cB];
		if (cA != rB)
		{
			Console.WriteLine("matrik can't be multiplied !!");
		}
		else
		{
			for (int i = 0; i < rA; i++)
			{
				for (int j = 0; j < cB; j++)
				{
					temp = 0.0f;
					for (int k = 0; k < cA; k++)
					{
						temp += A[i, k] * B[k, j];
					}
					res[i, j] = temp;
				}
			}
		}
		return res;
	}
}
