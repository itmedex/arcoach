﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script provides the orientation for the body parts by applying an orthogonalization
 * technique called Eberly.
 * 
 * This method only resort to two points instead of three points used in the 3Points method.
 * The logic to convert the vectors to quaternions is the same as the previous method.
 * 
 * Attach this script to all body parts in Body Parts (Eberly). Then for each part insert 
 * the two joints more adequate for the movement. 
 */

public class Eberly : MonoBehaviour {

	public GameObject firstJoint;
	public GameObject secondJoint;
	
	private Transform firstJointTrans;
	private Transform secondJointTrans;

	private Vector3 n;
	private Vector3 t;
	private Vector3 b;

	private Matrix4x4 m;
	private Transform trans;

	void Start () 
	{
		trans = GetComponent<Transform> ();
	}
	
	void Update () 
	{
		firstJointTrans = firstJoint.transform;
		secondJointTrans = secondJoint.transform;
		
		n = firstJointTrans.position - secondJointTrans.position;
		n = Vector3.Normalize (n);

		if (Mathf.Abs (n.x) >= Mathf.Abs (n.y)) 
		{
			t = new Vector3 (- n.z / Mathf.Sqrt (Mathf.Pow (n.x, 2) + Mathf.Pow (n.z, 2)), 
			                 0.0f,
			                 n.x / Mathf.Sqrt (Mathf.Pow (n.x, 2) + Mathf.Pow (n.z, 2)));

			b = new Vector3 ((n.x * n.y) / Mathf.Sqrt (Mathf.Pow (n.x, 2) + Mathf.Pow (n.z, 2)), 
			                 - Mathf.Sqrt (Mathf.Pow (n.x, 2) + Mathf.Pow (n.z, 2)),
			                 (n.y * n.z) / Mathf.Sqrt (Mathf.Pow (n.x, 2) + Mathf.Pow (n.z, 2)));
		} else 
		{
			t = new Vector3 (0.0f, 
			                 n.z / Mathf.Sqrt (Mathf.Pow (n.y, 2) + Mathf.Pow (n.z, 2)),
			                 - n.y / Mathf.Sqrt (Mathf.Pow (n.y, 2) + Mathf.Pow (n.z, 2)));
			
			b = new Vector3 (- Mathf.Sqrt (Mathf.Pow (n.y, 2) + Mathf.Pow (n.z, 2)), 
			                 (n.x * n.y) / Mathf.Sqrt (Mathf.Pow (n.y, 2) + Mathf.Pow (n.z, 2)),
			                 (n.x * n.z) / Mathf.Sqrt (Mathf.Pow (n.y, 2) + Mathf.Pow (n.z, 2)));
		}

		t = Vector3.Normalize (t);
		b = Vector3.Normalize (b);

		m.SetColumn (0, -t);
		m.SetColumn (1, n);
		m.SetColumn (2, b);

		trans.rotation = QuaternionFromMatrix (m);

	}
	
	public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
	{ 
		return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
	} 
}
