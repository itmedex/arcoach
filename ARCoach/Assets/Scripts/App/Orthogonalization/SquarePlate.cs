﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script provides the orientation for the body parts by applying an orthogonalization
 * technique called SquarePlate.
 * 
 * This method only resort to two points instead of three points used in the 3Points method.
 * The logic to convert the vectors to quaternions is the same as the previous method.
 * 
 * Attach this script to all body parts in Body Parts (Square Plate). Then for each part insert 
 * the two joints more adequate for the movement. 
 */

public class SquarePlate : MonoBehaviour {

	public GameObject firstJoint;
	public GameObject secondJoint;
	
	private Transform firstJointTrans;
	private Transform secondJointTrans;
	
	private Vector3 n;
	private Vector3 v;
	private Vector3 t;
	private Vector3 b;

	private Vector3 normn;
	private Vector3 normt;
	private Vector3 normb;
		
	private Matrix4x4 m;
	private Transform trans;

	void Start ()
	{
		trans = GetComponent<Transform> ();
	}
	
	void Update () 
	{
		firstJointTrans = firstJoint.transform;
		secondJointTrans = secondJoint.transform;
		
		n = firstJointTrans.position - secondJointTrans.position;

		n = Vector3.Normalize (n);

		if ((Mathf.Abs (n.x) >= 0.0f && Mathf.Abs (n.y) >= 0.0f) || (Mathf.Abs (n.x) <= 0.0f && Mathf.Abs (n.y) <= 0.0f)) {
			v = new Vector3 (n.x + 1,
			                 n.y - 1,
			                 n.z);
		} else {
			v = new Vector3 (n.x - 1,
			                 n.y - 1,
			                 n.z);
		}

		t = Vector3.Cross (v, n);

		normn = Vector3.Normalize (n);
		normt = Vector3.Normalize (t);

		b = Vector3.Cross (normt, normn);
		normb = Vector3.Normalize (b);

		m.SetColumn (0, -normt);
		m.SetColumn (1, normn);
		m.SetColumn (2, normb);
	
		trans.rotation = QuaternionFromMatrix (m);
	}
	
	public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
	{ 
		return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
	} 
}
