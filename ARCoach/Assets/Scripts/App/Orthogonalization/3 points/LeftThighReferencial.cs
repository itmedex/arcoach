﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script provides the orientation for the LeftThigh by applying an orthogonalization
 * technique with three joints (points).
 * 
 * In this method two vectors are calculated between the outer points and the middle point. One 
 * of these vectors will be the y axis. By performing the cross product between the two vectors, the 
 * axis z will arise. Finally, perform the cross product between the z and y vector to obtain the x
 * axis.
 * 
 * The rotations in Unity are done by quartenions and therefore a function (QuaternionFromMatrix) was made 
 * to convert the vectors obtained into a quaternion. This script is attached to the respective body part 
 * in Body Parts (3 Points). The three points that should be added in this case are the spinebase, 
 * lefthip and leftankle.
 */

public class LeftThighReferencial : MonoBehaviour {

	public GameObject spineBase;
	public GameObject leftHip;
	public GameObject leftKnee;
	
	private Transform spineBasetrans;
	private Transform leftHiptrans;
	private Transform leftKneetrans;
	
	private Vector3 spineBaseLeftHip;
	private Vector3 leftHipLeftKnee;
	
	private Vector3 x;
	private Vector3 y;
	private Vector3 z;
	
	private Vector3 normx;
	private Vector3 normy;
	private Vector3 normz;
	
	private Transform trans;
	
	private Matrix4x4 m;
	
	void Start () 
	{
		trans = GetComponent<Transform> ();
	}
	
	
	void Update () 
	{
		spineBasetrans = spineBase.transform;
		leftHiptrans = leftHip.transform;
		leftKneetrans = leftKnee.transform;
		
		spineBaseLeftHip = spineBasetrans.position - leftHiptrans.position;
		leftHipLeftKnee = leftKneetrans.position - leftHiptrans.position;
		
		z = Vector3.Cross (leftHipLeftKnee, spineBaseLeftHip);
		y = leftHipLeftKnee;
		
		normz = Vector3.Normalize (z);
		normy = Vector3.Normalize (y);
		
		x = Vector3.Cross (normy, normz);
		
		normx = Vector3.Normalize (x);
		
		m.SetColumn (0, normx);
		m.SetColumn (1, normy);
		m.SetColumn (2, normz);
		
		trans.rotation = QuaternionFromMatrix (m);
	}
	
	public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
	{ 
		return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
	}
}
