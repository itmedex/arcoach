﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script provides the orientation for the Torso by applying an orthogonalization
 * technique with three joints (points).
 * 
 * In this method two vectors are calculated between the outer points and the middle point. One 
 * of these vectors will be the y axis. By performing the cross product between the two vectors, the 
 * axis x will arise. Finally, perform the cross product between the x and y vector to obtain the z
 * axis.
 * 
 * The rotations in Unity are done by quartenions and therefore a function (QuaternionFromMatrix) was made 
 * to convert the vectors obtained into a quaternion. This script is attached to the respective body part 
 * in Body Parts (3 Points). The three points that should be added in this case are the spineshoulder,
 * spinemid and spinebase.
 */

public class TorsoReferencial : MonoBehaviour {

	public GameObject spineShoulder;
	public GameObject spineMid;
	public GameObject spineBase;
	
	private Transform spineShouldertrans;
	private Transform spineMidtrans;
	private Transform spineBasetrans;
	
	private Vector3 spineShoulderSpineMid;
	private Vector3 spineMidSpineBase; 
	
	private Vector3 x;
	private Vector3 y;
	private Vector3 z;
	
	private Vector3 normx;
	private Vector3 normy;
	private Vector3 normz;
	
	private Transform trans;
	
	private Matrix4x4 m;
	
	void Start()
	{
		trans = GetComponent<Transform> ();
	}
	
	void Update()
	{
		spineShouldertrans = spineShoulder.transform;
		spineMidtrans = spineMid.transform;
		spineBasetrans = spineBase.transform;

		spineShoulderSpineMid = spineShouldertrans.position - spineMidtrans.position;
		spineMidSpineBase = spineBasetrans.position - spineMidtrans.position;
		
		x = Vector3.Cross (spineShoulderSpineMid, spineMidSpineBase);
		y = spineShouldertrans.position - spineMidtrans.position;
		
		normx = Vector3.Normalize (x);
		normy = Vector3.Normalize (y);
		
		z = Vector3.Cross (normx, normy);
		
		normz = Vector3.Normalize (z);
		
		m.SetColumn (0, normx);
		m.SetColumn (1, normy);
		m.SetColumn (2, normz);
		
		trans.rotation = QuaternionFromMatrix (m);
	}
	
	public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
	{ 
		return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
	}
}
