﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script provides the orientation for the LeftArm by applying an orthogonalization
 * technique with three joints (points).
 * 
 * In this method two vectors are calculated between the outer points and the middle point. One 
 * of these vectors will be the y axis. By performing the cross product between the two vectors, the 
 * axis z will arise. Finally, perform the cross product between the z and y vector to obtain the x
 * axis.
 * 
 * The rotations in Unity are done by quartenions and therefore a function (QuaternionFromMatrix) was made 
 * to convert the vectors obtained into a quaternion. This script is attached to the respective body part 
 * in Body Parts (3 Points). The three points that should be added in this case are the spineshoulder, 
 * leftshoulder and leftelbow.
 */

public class LeftArmReferencial : MonoBehaviour {
	
	public GameObject spineShoulder;
	public GameObject leftShoulder;
	public GameObject leftElbow;
	
	private Transform spineShouldertrans;
	private Transform leftShouldertrans;
	private Transform leftElbowtrans;
	
	private Vector3 spineShoulderLeftShoulder;
	private Vector3 leftShoulderLeftElbow;
	
	private Vector3 x;
	private Vector3 y;
	private Vector3 z;
	
	private Vector3 normx;
	private Vector3 normy;
	private Vector3 normz;
	
	private Transform trans;
	
	private Matrix4x4 m;
	
	void Start () 
	{
		trans = GetComponent<Transform> ();
	}
	
	
	void Update () 
	{
		spineShouldertrans = spineShoulder.transform;
		leftShouldertrans = leftShoulder.transform;
		leftElbowtrans = leftElbow.transform;
		
		spineShoulderLeftShoulder = spineShouldertrans.position - leftShouldertrans.position;
		leftShoulderLeftElbow = leftElbowtrans.position - leftShouldertrans.position;
		
		z = Vector3.Cross (leftShoulderLeftElbow, spineShoulderLeftShoulder);
		y = leftShoulderLeftElbow;
		
		normz = Vector3.Normalize (z);
		normy = Vector3.Normalize (y);
		
		x = Vector3.Cross (normy, normz);
		
		normx = Vector3.Normalize (x);
		
		m.SetColumn (0, normx);
		m.SetColumn (1, normy);
		m.SetColumn (2, normz);
		
		trans.rotation = QuaternionFromMatrix (m);
	}
	
	public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
	{ 
		return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
	}
}
