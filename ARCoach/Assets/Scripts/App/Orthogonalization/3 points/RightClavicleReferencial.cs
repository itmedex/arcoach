﻿using UnityEngine;
using System.Collections;

public class RightClavicleReferencial : MonoBehaviour {

	public GameObject neck;
	public GameObject spineShoulder;
	public GameObject rightShoulder;

	private Transform necktrans;
	private Transform spineShouldertrans;
	private Transform rightShouldertrans;

	private Vector3 neckSpineShoulder;
	private Vector3 spineShoulderRightShoulder;

	private Vector3 x;
	private Vector3 y;
	private Vector3 z;
	
	private Vector3 normx;
	private Vector3 normy;
	private Vector3 normz;
	
	private Transform trans;
	
	private Matrix4x4 m;
	
	void Start () 
	{
		trans = GetComponent<Transform> ();
	}
	
	
	void Update () 
	{
		necktrans = neck.transform;
		spineShouldertrans = spineShoulder.transform;
		rightShouldertrans = rightShoulder.transform;

		neckSpineShoulder = necktrans.position - spineShouldertrans.position;
		spineShoulderRightShoulder = rightShouldertrans.position - spineShouldertrans.position;
				
		z = Vector3.Cross (spineShoulderRightShoulder, neckSpineShoulder);
		y = spineShoulderRightShoulder;
		
		normz = Vector3.Normalize (z);
		normy = Vector3.Normalize (y);
		
		x = Vector3.Cross (normy, normz);
		
		normx = Vector3.Normalize (x);
		
		m.SetColumn (0, normx);
		m.SetColumn (1, normy);
		m.SetColumn (2, normz);
		
		trans.rotation = QuaternionFromMatrix (m);
	}
	
	public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
	{ 
		return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
	}
}
