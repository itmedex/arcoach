﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script provides the orientation for the RightThigh by applying an orthogonalization
 * technique with three joints (points).
 * 
 * In this method two vectors are calculated between the outer points and the middle point. One 
 * of these vectors will be the y axis. By performing the cross product between the two vectors, the 
 * axis z will arise. Finally, perform the cross product between the z and y vector to obtain the x
 * axis.
 * 
 * The rotations in Unity are done by quartenions and therefore a function (QuaternionFromMatrix) was made 
 * to convert the vectors obtained into a quaternion. This script is attached to the respective body part 
 * in Body Parts (3 Points). The three points that should be added in this case are the spinebase, 
 * righthip and rightankle.
 */

public class RightThighReferencial : MonoBehaviour {

	public GameObject spineBase;
	public GameObject rightHip;
	public GameObject rightKnee;
	
	private Transform spineBasetrans;
	private Transform rightHiptrans;
	private Transform rightKneetrans;
	
	private Vector3 spineBaseRightHip;
	private Vector3 rightHipRightKnee;
	
	private Vector3 x;
	private Vector3 y;
	private Vector3 z;
	
	private Vector3 normx;
	private Vector3 normy;
	private Vector3 normz;
	
	private Transform trans;
	
	private Matrix4x4 m;
	
	void Start () 
	{
		trans = GetComponent<Transform> ();
	}
	
	
	void Update () 
	{
		spineBasetrans = spineBase.transform;
		rightHiptrans = rightHip.transform;
		rightKneetrans = rightKnee.transform;
		
		spineBaseRightHip = spineBasetrans.position - rightHiptrans.position;
		rightHipRightKnee = rightKneetrans.position - rightHiptrans.position;
		
		z = Vector3.Cross (spineBaseRightHip, rightHipRightKnee);
		y = rightHipRightKnee;
		
		normz = Vector3.Normalize (z);
		normy = Vector3.Normalize (y);
		
		x = Vector3.Cross (normz, normy);
		
		normx = Vector3.Normalize (x);
		
		m.SetColumn (0, normx);
		m.SetColumn (1, normy);
		m.SetColumn (2, normz);
		
		trans.rotation = QuaternionFromMatrix (m);
	}
	
	public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
	{ 
		return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
	}
}
