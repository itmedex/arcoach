﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script provides the orientation for the LeftForeArm by applying an orthogonalization
 * technique with three joints (points).
 * 
 * In this method two vectors are calculated between the outer points and the middle point. One 
 * of these vectors will be the y axis. By performing the cross product between the two vectors, the 
 * axis x will arise. Finally, perform the cross product between the x and y vector to obtain the z
 * axis.
 * 
 * The rotations in Unity are done by quartenions and therefore a function (QuaternionFromMatrix) was made 
 * to convert the vectors obtained into a quaternion. This script is attached to the respective body part 
 * in Body Parts (3 Points). The three points that should be added in this case are the leftshoulder, 
 * leftelbow and leftwrist.
 */

public class LeftForeArmReferencial : MonoBehaviour {

	public GameObject leftShoulder;
	public GameObject leftElbow;
	public GameObject leftWrist;
	
	private Transform leftShouldertrans;
	private Transform leftElbowtrans;
	private Transform leftWristtrans;
	
	private Vector3 leftShoulderLeftElbow;
	private Vector3 leftElbowLeftWrist;
	
	private Vector3 x;
	private Vector3 y;
	private Vector3 z;
	
	private Vector3 normx;
	private Vector3 normy;
	private Vector3 normz;
	
	private Transform trans;
	
	private Matrix4x4 m;
	
	void Start () 
	{
		trans = GetComponent<Transform> ();
	}
	
	
	void Update () 
	{
		leftShouldertrans = leftShoulder.transform;
		leftElbowtrans = leftElbow.transform;
		leftWristtrans = leftWrist.transform;
		
		leftShoulderLeftElbow = leftShouldertrans.position - leftElbowtrans.position;
		leftElbowLeftWrist = leftWristtrans.position - leftElbowtrans.position;
		
		x = Vector3.Cross (leftElbowLeftWrist, leftShoulderLeftElbow);
		y = leftElbowLeftWrist;
		
		normx = Vector3.Normalize (x);
		normy = Vector3.Normalize (y);
		
		z = Vector3.Cross (normx, normy);
		
		normz = Vector3.Normalize (z);
		
		m.SetColumn (0, normx);
		m.SetColumn (1, normy);
		m.SetColumn (2, normz);
		
		trans.rotation = QuaternionFromMatrix (m);
	}
	
	public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
	{ 
		return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
	}
}
