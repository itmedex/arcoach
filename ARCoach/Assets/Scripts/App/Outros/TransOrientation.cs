﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script sets the orientation to the GameObject.
 * 
 * It receives one joint whose orientation (Kinect's orientation) will be the same for the
 * GameObject.
 * 
 * Attach this script to the body parts in Body Parts (Kinect).
 */

public class TransOrientation : MonoBehaviour {

	public GameObject joint;
	
	private Transform trans;

	private Transform jointtrans;
	
	void Start () 
	{
		trans = GetComponent<Transform> ();
	}
	
	void Update ()
	{
		jointtrans = joint.transform;
		trans.rotation = jointtrans.rotation; 	
	}
}
