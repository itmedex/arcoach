﻿using UnityEngine;
using System.Collections;

public class DistanceBehavior : MonoBehaviour {

	private Transform trans;
	private Vector3 pos;

	private float zpos;

	private Renderer rend;

	void Start () 
	{
		trans = GetComponent<Transform> ();
		rend = GetComponent<Renderer> ();
	}

	void Update () 
	{
		pos = trans.position;
		zpos = pos.z;

		if (zpos < -4f) {
			rend.material.color = Color.black;
		} 
		else if (-4f <= zpos && zpos < -3f)
		{
			rend.material.color = Color.red;
		}
		else if (-3f <= zpos && zpos < -2f) 
		{
			rend.material.color = Color.green;
		} 
		else if (-2f <= zpos && zpos < -1f) 
		{
			rend.material.color = Color.blue;
		} 
		else if (-1f <= zpos && zpos < 0f) 
		{
			rend.material.color = Color.yellow;
		}

	}



}
