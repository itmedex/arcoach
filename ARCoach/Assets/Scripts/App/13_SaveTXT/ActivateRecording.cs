﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script activates/deactivates the recording of the orientation and position data.
 * 
 * Attach this script to the body parts you have already attached the scripts SaveTxt and SaveTxtPos.
 */

public class ActivateRecording : MonoBehaviour {

	public void DataRecordingOn()
	{
		if (gameObject.GetComponent<SaveTxt> ().enabled == false) 
		{
			gameObject.GetComponent<SaveTxt> ().enabled = true;
		} else 
		{
			gameObject.GetComponent<SaveTxt> ().enabled = false;
		}
	}

	public void DataRecordingPositionsOn()
	{
		if (gameObject.GetComponent<SaveTxtPos> ().enabled == false) 
		{
			gameObject.GetComponent<SaveTxtPos> ().enabled = true;
		} else 
		{
			gameObject.GetComponent<SaveTxtPos> ().enabled = false;
		}
	}
}
