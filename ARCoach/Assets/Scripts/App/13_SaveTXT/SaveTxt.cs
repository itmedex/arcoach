﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

/*
 * DESCRIPTION
 * 
 * This script saves the orientation data of the GameObject this script is attached to into
 * a Text file.
 * 
 * Attach this to the body parts you want to save data.
 */

public class SaveTxt : MonoBehaviour {

	private string path;

	private string gameobjectname;

	private Transform trans;

	private Vector3 angles;

	private string text;

	void Start () 
	{   
        if (gameObject.transform.parent.gameObject.activeSelf) {
            gameobjectname = gameObject.name;

            Directory.CreateDirectory(@".\..\Recorded\");
            Directory.CreateDirectory(@".\..\Recorded\Orientations");

            path = @".\..\Recorded\Orientations\" + gameobjectname + ".txt";

            //File.WriteAllText (path, "Orientation in Euler Angles (x, y , z)" + "\r\n");

            //File.WriteAllText (path, 1 + " " + 2 + " " + 3 + "\r\n");
        }

    }
	
	void Update () 
	{
        if (gameObject.transform.parent.gameObject.activeSelf) {

            trans = gameObject.transform;

            angles = trans.localEulerAngles;

            string text = angles.x.ToString("G4") + " " + angles.y.ToString("G4") + " " + angles.z.ToString("G4") + "\r\n";

            File.AppendAllText(path, text);
        }
	}
}
