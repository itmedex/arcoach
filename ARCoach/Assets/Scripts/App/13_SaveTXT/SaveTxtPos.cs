﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

/*
 * DESCRIPTION
 * 
 * This script saves the position data of the GameObject this script is attached to into
 * a Text file.
 * 
 * Attach this to the body parts you want to save data.
 */

public class SaveTxtPos : MonoBehaviour {

	private string path;
	
	private string gameobjectname;
	
	private Transform trans;
	
	private Vector3 pos;

	private string text;
	
	void Start () 
	{
        if (gameObject.transform.parent.gameObject.activeSelf) {

            gameobjectname = gameObject.name;

            Directory.CreateDirectory(@".\..\Recorded\");
            Directory.CreateDirectory(@".\..\Recorded\Positions");

            path = @".\..\Recorded\Positions\" + gameobjectname + ".txt";

            //File.WriteAllText (path, "Position (x, y , z)" + "\r\n");

            //File.WriteAllText (path, 1 + " " + 2 + " " + 3 + "\r\n");
        }
	}
	
	void Update () 
	{
        if (gameObject.transform.parent.gameObject.activeSelf) {

            trans = gameObject.transform;

            pos = trans.localPosition;

            string text = pos.x.ToString("G4") + " " + pos.y.ToString("G4") + " " + pos.z.ToString("G4") + "\r\n";

            File.AppendAllText(path, text);
        }
	}
}
