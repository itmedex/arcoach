﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script activates or deactivates the first child of a GameObject when this 
 * GameObject is clicked. 
 * 
 * Attach this to every body part of all the methods of orthogonalization to activate/deactivate
 * the gizmo of that body part.
 */

public class ActivateGizmo : MonoBehaviour {

	private GameObject child;

	void Start()
	{
		child = gameObject.transform.GetChild (0).gameObject;
	}

	void OnMouseDown()
	{
		if (child.activeSelf == false)
		{
			child.SetActive (true);
		} else if (child.activeSelf == true) 
		{
			child.SetActive(false);
		}

	}
}
