﻿using UnityEngine;
using System.Collections;

/*
 * DESCRIPTION
 * 
 * This script sets the position and orientation of this GameObject by taking those parameters
 * of the parent of the parent of this GameObject.
 * 
 * Attach this to the Center on all Gizmos with the aim of each gizmo have the same orientation
 * as the respective body part.
 * */

public class ParentPosOri : MonoBehaviour {

	private GameObject parent;

	private Transform parentTrans;

	private Transform childTrans;

	void Start () 
	{
		childTrans = GetComponent<Transform> ();
	}
	
	void Update () 
	{
		parentTrans = gameObject.transform.parent.parent.GetComponent<Transform>();

		childTrans.position = parentTrans.position;
		childTrans.rotation = parentTrans.rotation;
	}
}
