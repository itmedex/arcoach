﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowJoint : MonoBehaviour {

    public GameObject joint;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = joint.transform.position;
	}
}
