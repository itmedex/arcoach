﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThumbDirection : MonoBehaviour {

    public GameObject thumb;
    public GameObject handTip;
    public GameObject wrist;

    private Vector3 handThumb;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        handThumb = thumb.transform.position - wrist.transform.position;

        float yAxisAngle = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(handThumb.normalized, new Vector3(0, 1, 0)));
        float zAxisAngle = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(handThumb.normalized, new Vector3(0, 0, -1)));


        if (yAxisAngle < 50 || zAxisAngle < 30 ) {
            gameObject.GetComponent<Renderer>().material.color = new Color(0, 1, 0);

        }

        else {
            gameObject.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
        }

        gameObject.transform.up = handThumb;
    }
}
