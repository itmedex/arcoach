﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timeline : MonoBehaviour {
    private LineRenderer linerenderer;
    private int count;

    public GameObject background;
    private GameObject lines;

    public GameObject rightShoulder;
    public GameObject leftShoulder;

    private float verticalAngle;

    private float verticalDelta;

    private float xInitialPosition;
    private float yInitialPosition;

    private Vector3 lastPoint;
    private ArrayList linesArrayList = new ArrayList();

    private float pointScale;

    // Use this for initialization
    void Start() {
        Init();
    }

    void Init() {
        lines = new GameObject();
        lines.name = "Lines";
        lines.transform.parent = gameObject.transform;
        pointScale = background.transform.lossyScale.x / 50;
        InvokeRepeating("drawLine", 0, 0.05f);
    }

    void drawLine() {
        if (rightShoulder.transform.position == leftShoulder.transform.position)
            return;

        verticalAngle = Angles.getVerticalAngle();

        yInitialPosition = background.transform.position.y - background.transform.lossyScale.y / 2;
        xInitialPosition = background.transform.position.x + background.transform.lossyScale.x / 2;


        verticalDelta = background.transform.lossyScale.y * verticalAngle / 180;

        Vector3 newPoint;

        if (lastPoint != null) {
            newPoint = new Vector3(xInitialPosition, yInitialPosition + verticalDelta, background.transform.position.z - 1);

            if (Vector3.Distance(newPoint, lastPoint) > background.transform.lossyScale.x / 3) {
                lastPoint = newPoint;
                return;
            }

            if (linesArrayList.Count > 0) {
                GameObject lastLine = (GameObject)linesArrayList[0];

                //Debug.Log("oldLine: " + (oldLine.transform.position.x) + " lastLine: " + lastLine.transform.position.x);
                //Debug.Log("old: " + Mathf.Abs(lastLine.transform.position.x -oldLine.transform.position.x) + " limit: " + background.transform.lossyScale.x);

                for (int i = 0; i < linesArrayList.Count; i++) {
                    GameObject lineTemp = (GameObject)linesArrayList[i];
                    lineTemp.transform.position = new Vector3(lineTemp.transform.position.x - pointScale, lineTemp.transform.position.y, lineTemp.transform.position.z);

                    if (Mathf.Abs(lastLine.transform.position.x - lineTemp.transform.position.x) >= background.transform.lossyScale.x) {
                        //Debug.Log("ENTREI");
                        linesArrayList.Remove(lineTemp);
                        Destroy(lineTemp);
                    }
                }
            }

            lastPoint.x -= pointScale;

            GameObject line = new GameObject();
            line.name = "Line";
            line.transform.parent = lines.transform;

            linesArrayList.Insert(0, line);

            LineRenderer lineRenderer = line.AddComponent<LineRenderer>();
            lineRenderer.startWidth = 0.01f;
            lineRenderer.endWidth = 0.01f;
            lineRenderer.useWorldSpace = false;
            if (Math.Abs(leftShoulder.transform.position.y - rightShoulder.transform.position.y) > 0.1) {
                lineRenderer.material = Resources.Load("Materials/Timeline/Line bad", typeof(Material)) as Material;
            }
            else {
                lineRenderer.material = Resources.Load("Materials/Timeline/Line", typeof(Material)) as Material;
            }

            lineRenderer.SetPosition(0, lastPoint);
            lineRenderer.SetPosition(1, newPoint);
        }
        else {
            newPoint = new Vector3(xInitialPosition, yInitialPosition, background.transform.position.z - 1);
        }

        lastPoint = newPoint;
    }

    // Update is called once per frame
    void LateUpdate() {
        
    }

    public void Restart() {
        Destroy(lines);
        Init();
        CancelInvoke("drawLine");
    }
}
