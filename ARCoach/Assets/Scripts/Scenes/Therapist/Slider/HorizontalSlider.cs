﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HorizontalSlider : MonoBehaviour {

    public GameObject bar;
    public GameObject background;

    public Text text;

    private float angle;
    private float delta;
    private float xInitialPosition;

	// Use this for initialization
	void Start () {
	}
	
	void LateUpdate () {
        angle = Angles.getHorizontalAngle();
        text.text = angle + "º";

        delta = background.transform.lossyScale.x * angle / 180;
        xInitialPosition = background.transform.position.x - background.transform.lossyScale.x / 2;

        bar.transform.position = new Vector3(xInitialPosition + delta, bar.transform.position.y, bar.transform.position.z);
    }
}
