﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VerticalSlider : MonoBehaviour {

    public GameObject bar;
    public GameObject background;

    public Text text;

    private float angle;
    private float delta;
    private float yInitialPosition;

    // Use this for initialization
    void Start() {
    }

    void Update() {
        angle = Angles.getVerticalAngle();
        text.text = angle + "º";

        delta = background.transform.lossyScale.y * angle / 180;
        yInitialPosition = background.transform.position.y - background.transform.lossyScale.y / 2;

        bar.transform.position = new Vector3(bar.transform.position.x, yInitialPosition + delta, bar.transform.position.z);

    }
}
