﻿using UnityEngine;
using System.Collections;
using System;

/*
 * DESCRIPTION
 * 
 * This script will draw a line between two joints.
 * 
 * With this script we aim to obtain a simple representation of the skeleton by using
 * line renderers. This was done by defining the two joints which will connect the line.
 * 
 * Attach this script to the GameObjects in LineRenderer.
 */

public class DrawBones : MonoBehaviour {

	private LineRenderer linerenderer;
	
	public Transform origin;
	public Transform destination;

    private GameObject sphere1;
    private GameObject sphere2;
    private Vector3 vector;
    private ArrayList spherePath = new ArrayList();
    private float distance;
    private float radius;
    private bool isFirstPosition = true;
    Vector3 positionTemp;
    Vector3 increment;
    private bool spheresOn = false;

    void Start()
	{
		linerenderer = GetComponent<LineRenderer> ();
        if(spheresOn) {
            sphere1 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);

            sphere1.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            sphere2.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        }
        
    }

    void LateUpdate()
	{
        if(spheresOn) {
            sphere1.transform.position = origin.position;
            sphere2.transform.position = destination.position;
            sphere1.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
            sphere2.GetComponent<Renderer>().material.SetColor("_Color", Color.red);

            if (sphere1.transform.position != sphere2.transform.position) {
                vector = sphere1.transform.position - sphere2.transform.position;
                distance = Vector3.Distance(sphere1.transform.position, sphere2.transform.position);
                radius = sphere1.GetComponent<Renderer>().bounds.extents.x;
                radius = radius - radius / 2;
                positionTemp = sphere2.transform.position;
                int sphereCount = (int)Math.Floor(distance / radius);
                increment = vector / sphereCount;

                if (isFirstPosition) {
                    for (int counter = 0; counter < sphereCount; counter++) {
                        GameObject sphereTemp = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        sphereTemp.transform.position = positionTemp;
                        sphereTemp.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
                        positionTemp = sphereTemp.transform.position + increment;
                        sphereTemp.transform.position = positionTemp;
                        spherePath.Add(sphereTemp);
                    }
                    isFirstPosition = false;
                }
                else {
                    foreach (GameObject sphereTemp in spherePath) {
                        sphereTemp.transform.position = positionTemp;
                        sphereTemp.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
                        positionTemp = sphereTemp.transform.position + increment;
                        sphereTemp.transform.position = positionTemp;
                    }
                }

            }
        }

        linerenderer.SetPosition (0, origin.position);
		linerenderer.SetPosition (1, destination.position);
	}

}
