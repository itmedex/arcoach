﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoulderBar : MonoBehaviour {

    public GameObject bar;
    public GameObject spineShoulder;
    public GameObject leftShoulder;
    public GameObject rightShoulder;
    public GameObject arrow;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        bar.transform.position = new Vector3(spineShoulder.transform.position.x, spineShoulder.transform.position.y, bar.transform.position.z);

        if (Math.Abs(leftShoulder.transform.position.y - rightShoulder.transform.position.y) > 0.075f) {
            bar.GetComponent<Renderer>().material.color = new Color(1, 0, 0, 0.3f);
            arrow.SetActive(true);

            if (leftShoulder.transform.position.y > rightShoulder.transform.position.y) {
                arrow.transform.position = new Vector3(leftShoulder.transform.position.x - 0.1f, leftShoulder.transform.position.y + 0.1f, leftShoulder.transform.position.z);
            }
            else {
                arrow.transform.position = new Vector3(rightShoulder.transform.position.x + 0.1f, rightShoulder.transform.position.y + 0.1f, rightShoulder.transform.position.z);

            }
        }
        else {
            bar.GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.3f);
            arrow.SetActive(false);
        }
    }
}
