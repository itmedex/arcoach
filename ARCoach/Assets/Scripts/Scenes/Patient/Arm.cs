﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arm : MonoBehaviour {
    private static bool leftArmSelected;

    public GameObject linechart;

	public void selectLeftArm() {
        leftArmSelected = true;
        linechart.SetActive(true);
        gameObject.SetActive(false);
    }

    public void selectRightArm() {
        leftArmSelected = false;
        linechart.SetActive(true);
        gameObject.SetActive(false);
    }

    public static bool isLeftArmSelected() {
        return leftArmSelected;
    }

    public static bool isRightArmSelected() {
        return !leftArmSelected;
    }
}
