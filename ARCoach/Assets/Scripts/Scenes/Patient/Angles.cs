﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Angles : MonoBehaviour {
    private static float horizontalAngle;
    private static float verticalAngle;

    public GameObject spineShoulder;
    public GameObject spineMid;
    public GameObject rightShoulder;
    public GameObject leftShoulder;
    public GameObject rightElbow;
    public GameObject leftElbow;

    private Vector3 rightShoulderLeftShoulder;
    private Vector3 spineShoulderSpineMid;
    private Vector3 shoulderElbow;

    private void Update() {
        spineShoulderSpineMid = spineMid.transform.position - spineShoulder.transform.position;

        if (Arm.isRightArmSelected()) {
            shoulderElbow = rightElbow.transform.position - rightShoulder.transform.position;
        }
        else if (Arm.isLeftArmSelected()) {
            shoulderElbow = leftElbow.transform.position - leftShoulder.transform.position;
        }

        rightShoulderLeftShoulder = leftShoulder.transform.position - rightShoulder.transform.position;

        horizontalAngle = Mathf.Rad2Deg * Mathf.Acos(Vector2.Dot(new Vector2(rightShoulderLeftShoulder.x, rightShoulderLeftShoulder.z).normalized, new Vector2(shoulderElbow.x, shoulderElbow.z).normalized));
        verticalAngle = Mathf.Rad2Deg * Mathf.Acos(Vector2.Dot(new Vector2(spineShoulderSpineMid.y, spineShoulderSpineMid.z).normalized, new Vector2(shoulderElbow.y, shoulderElbow.z).normalized));
    }

    public static float getHorizontalAngle() {
        return horizontalAngle;
    }

    public static float getVerticalAngle() {
        return verticalAngle;
    }
}
