﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawLine : MonoBehaviour {
    public GameObject background;
    public GameObject startButton;
    public GameObject restartButton;
    public GameObject cursor;
    public GameObject armButtons;
    public Text timer;
    public Text counter;
    public Text selectTargets;
    public Text sessionTime;

    public GameObject bodyImageRight;
    public GameObject bodyImageLeft;

    private int timerInt;
    private int counterInt;
    private int sessionTimeInt;

    private int count;
    private LineRenderer lineRenderer;
    private GameObject lines;
    private GameObject targets;
    private bool hasDefinedTargets;
    private Vector3 lastPoint;

    private float horizontalDelta;
    private float verticalDelta;

    private float xInitialPosition;
    private float yInitialPosition;

    private int currentTarget;
    private bool isGroing;
    private AudioClip beep;
    private AudioSource audioSource;

    private float pointScale;

    // Use this for initialization
    void Start () {
        Init();
        beep = (AudioClip)Resources.Load("Sounds/beep");
        audioSource = GetComponent<AudioSource>();
        pointScale = background.transform.lossyScale.x / 300;
    }

    void Init() {
        lines = new GameObject();
        lines.name = "Lines";
        lines.transform.parent = gameObject.transform;

        targets = new GameObject();
        targets.name = "Targets";
        targets.transform.parent = gameObject.transform;

        timerInt = 4;
        currentTarget = 0;
        counterInt = 0;
        sessionTimeInt = 0;
    }

    bool OutOfBounds(float xMin, float xMax, float yMin, float yMax, Vector3 position) {
        if (position.x < xMin || position.x > xMax || position.y < yMin || position.y > yMax) {
            return true;
        }
        return false;
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (!hasDefinedTargets) {
            if (Arm.isLeftArmSelected()) {
                bodyImageLeft.SetActive(true);
            }
            else if (Arm.isRightArmSelected()) {
                bodyImageRight.SetActive(true);
            }
        }

        if (!hasDefinedTargets && Input.GetMouseButtonDown(0)) {
            if (Arm.isLeftArmSelected()) {
                bodyImageLeft.SetActive(true);
            }
            else if (Arm.isRightArmSelected()) {
                bodyImageRight.SetActive(true);
            }

            Vector3 posVec = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            posVec.z = transform.position.z;

            float xMin = background.transform.position.x - background.transform.lossyScale.x / 2;
            float xMax = background.transform.position.x + background.transform.lossyScale.x / 2;
            float yMin = background.transform.position.y - background.transform.lossyScale.y / 2;
            float yMax = background.transform.position.y + background.transform.lossyScale.y / 2;

            if (!OutOfBounds(xMin, xMax, yMin, yMax, posVec)) {
                GameObject target = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                target.name = "Target";
                target.GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
                target.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
                target.transform.parent = targets.transform;
                target.transform.position = posVec;
             }
        }
        if (hasDefinedTargets && timerInt == 0) {
            bodyImageLeft.SetActive(false);
            bodyImageRight.SetActive(false);

            yInitialPosition = background.transform.position.y - background.transform.lossyScale.y / 2;
            xInitialPosition = background.transform.position.x - background.transform.lossyScale.x / 2;

            horizontalDelta = background.transform.lossyScale.x * Angles.getHorizontalAngle() / 180;
            verticalDelta = background.transform.lossyScale.y * Angles.getVerticalAngle() / 180;

            Vector3 newPoint;
            if (lastPoint != null) {
                newPoint = new Vector3(xInitialPosition + horizontalDelta, yInitialPosition + verticalDelta, background.transform.position.z - 4);

                if (Vector3.Distance(newPoint, lastPoint) > background.transform.lossyScale.x / 3 || Vector3.Distance(newPoint, lastPoint) < pointScale) {
                    lastPoint = newPoint;
                    return;
                }

                GameObject line = new GameObject();
                line.name = "Line";
                line.transform.parent = lines.transform;

                LineRenderer lineRenderer = line.AddComponent<LineRenderer>();
                lineRenderer.startWidth = 0.01f;
                lineRenderer.endWidth = 0.01f;
                lineRenderer.useWorldSpace = false;
                lineRenderer.material = Resources.Load("Materials/Linechart/Line", typeof(Material)) as Material;
                lineRenderer.SetPosition(0, lastPoint);
                lineRenderer.SetPosition(1, newPoint);

                cursor.transform.position = newPoint;
            } else {
                newPoint = new Vector3(xInitialPosition, yInitialPosition, background.transform.position.z + 5);
            }

            lastPoint = newPoint;

            if (targets.transform.childCount == 0)
                return;
            Vector3 targetPosition = targets.transform.GetChild(currentTarget).position;

            if (Vector2.Distance(new Vector2(targetPosition.x, targetPosition.y), new Vector2(cursor.transform.position.x, cursor.transform.position.y)) < 0.1) {
                targets.transform.GetChild(currentTarget).gameObject.GetComponent<Renderer>().material.color = new Color(1, 1, 1);
                if (currentTarget == (targets.transform.childCount - 1)) {
                    currentTarget = 0;
                    counter.text = "" + ++counterInt;
                }
                else {
                    currentTarget++;
                }
                audioSource.PlayOneShot(beep);
            }
        }        
    }

    public void Restart() {
        Destroy(lines);
        Destroy(targets);
        startButton.SetActive(true);
        selectTargets.gameObject.SetActive(true);
        restartButton.SetActive(false);
        hasDefinedTargets = false;
        gameObject.SetActive(false);
        armButtons.SetActive(true);
        cursor.SetActive(false);
        counter.gameObject.SetActive(false);
        sessionTime.gameObject.SetActive(false);
        timer.gameObject.SetActive(false);
        CancelInvoke();
        Init();
    }

    public void StartTherapy() {
        if (targets.transform.childCount > 1) {
            hasDefinedTargets = true;
            startButton.SetActive(false);
            selectTargets.gameObject.SetActive(false);
            restartButton.SetActive(true);
            timer.gameObject.SetActive(true);
            InvokeRepeating("countDown", 0, 1);
        }
    }

    private void countDown() {
        if(timerInt > 1) {
            timerInt--;
            timer.text = "" + timerInt;
        }
        else {
            timer.gameObject.SetActive(false);
            timerInt--;
            CancelInvoke("countDown");
            cursor.SetActive(true);
            counter.gameObject.SetActive(true);
            sessionTime.gameObject.SetActive(true);
            InvokeRepeating("blinkTarget", 0, 0.05f);
            InvokeRepeating("sessionTimeInc", 0, 1);
        }
    }

    private void sessionTimeInc() {
        sessionTimeInt++;
        int minutes = sessionTimeInt / 60;
        int seconds = sessionTimeInt % 60;
        sessionTime.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }

    private void blinkTarget() {
        if (targets.transform.childCount == 0)
            return;
        Renderer renderer = targets.transform.GetChild(currentTarget).gameObject.GetComponent<Renderer>();
        float delta;

        if (isGroing && renderer.material.color.r > 1) {
            isGroing = false;
        }
        else if (!isGroing && renderer.material.color.r < 0) {
            isGroing = true;
        }

        if (isGroing) {
            delta = 0.1f;
        }
        else {
            delta = -0.1f;
        }

        Color color = renderer.material.color;
        color.r += delta;
        color.g += delta;

        renderer.material.color = color;
    }
}
