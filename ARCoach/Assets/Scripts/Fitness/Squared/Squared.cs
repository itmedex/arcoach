﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Squared : MonoBehaviour {

    public GameObject leftHip;
    public GameObject rightHip;
    public GameObject leftShoulder;
    public GameObject rightShoulder;
    public GameObject leftKnee;
    public GameObject rightKnee;

    public GameObject spineShoulder;
    public GameObject spineBase;

    public GameObject shoulderBar;
    public GameObject hipBar;
    public GameObject kneeBar;

    public Text hipShoulderAngleText;
    public Text hipKneeAngleText;
    public Text shoulderKneeAngleText;

    private Vector3 rightHipLeftHip;
    private Vector3 rightShoulderLeftShoulder;
    private Vector3 rightKneeLeftKnee;

    private float hipShoulderAngle;
    private float hipKneeAngle;
    private float shoulderKneeAngle;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (leftHip.transform.position == rightHip.transform.position)
            return;

        shoulderBar.transform.position = new Vector3(spineShoulder.transform.position.x, spineShoulder.transform.position.y, spineShoulder.transform.position.z);
        hipBar.transform.position = new Vector3(spineBase.transform.position.x, spineBase.transform.position.y, spineBase.transform.position.z);
        kneeBar.transform.position = new Vector3(spineBase.transform.position.x, (rightKnee.transform.position.y + leftKnee.transform.position.y) / 2, rightKnee.transform.position.z);


        rightHipLeftHip = leftHip.transform.position - rightHip.transform.position;
        rightShoulderLeftShoulder = leftShoulder.transform.position - rightShoulder.transform.position;
        rightKneeLeftKnee = leftKnee.transform.position - rightKnee.transform.position;

        hipShoulderAngle = Mathf.Rad2Deg * Mathf.Acos(Vector2.Dot(new Vector2(rightShoulderLeftShoulder.x, rightShoulderLeftShoulder.z).normalized, new Vector2(rightHipLeftHip.x, rightHipLeftHip.z).normalized));
        hipKneeAngle = Mathf.Rad2Deg * Mathf.Acos(Vector2.Dot(new Vector2(rightKneeLeftKnee.x, rightKneeLeftKnee.z).normalized, new Vector2(rightHipLeftHip.x, rightHipLeftHip.z).normalized));
        shoulderKneeAngle = Mathf.Rad2Deg * Mathf.Acos(Vector2.Dot(new Vector2(rightKneeLeftKnee.x, rightKneeLeftKnee.z).normalized, new Vector2(rightShoulderLeftShoulder.x, rightShoulderLeftShoulder.z).normalized));

        hipShoulderAngleText.text = "hipShoulderAngle: " + hipShoulderAngle + "º";
        hipKneeAngleText.text = "hipKneeAngle: " + hipKneeAngle + "º";
        shoulderKneeAngleText.text = "shoulderKneeAngle: " + shoulderKneeAngle + "º";


        if (hipShoulderAngle > 5 && hipKneeAngle < 5 && shoulderKneeAngle > 5 || Math.Abs(leftShoulder.transform.position.y - rightShoulder.transform.position.y) > 0.04) {
            shoulderBar.GetComponent<Renderer>().material.color = new Color(1, 0, 0, 0.3f);
        }
        else {
            shoulderBar.GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.3f);
        }

        if (hipShoulderAngle > 5 && hipKneeAngle > 5 && shoulderKneeAngle < 5 || Math.Abs(leftHip.transform.position.y - rightHip.transform.position.y) > 0.02) {
            hipBar.GetComponent<Renderer>().material.color = new Color(1, 0, 0, 0.3f);
        }
        else {
            hipBar.GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.3f);

        }

        if (hipShoulderAngle < 5 && hipKneeAngle > 5 && shoulderKneeAngle > 5 || Math.Abs(leftKnee.transform.position.y - rightKnee.transform.position.y) > 0.2) {
            kneeBar.GetComponent<Renderer>().material.color = new Color(1, 0, 0, 0.3f);
        }
        else {
            kneeBar.GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.3f);
        }
    }
}
