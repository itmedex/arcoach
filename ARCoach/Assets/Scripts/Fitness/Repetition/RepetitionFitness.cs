﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RepetitionFitness : MonoBehaviour {

    public GameObject hip;
    public GameObject knee;
    public GameObject ankle;
    public GameObject spineMid;
    public GameObject spineBase;    

    public GameObject depthMarker;
    public Text text;

    private Vector3 kneeHip;
    private Vector3 hipKnee;
    private Vector3 kneeAnkle;
    private Vector3 spineBaseSpineMid;
    private float legAngle;
    private float hipAngle;
    private bool hasHitDepth = false;
    private int counter = 0;

    // Use this for initialization
    void Start() {
        depthMarker.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
    }

    // Update is called once per frame
    void LateUpdate() {
        if (hip.transform.position == knee.transform.position)
            return;

        kneeHip = hip.transform.position - knee.transform.position;
        kneeAnkle = ankle.transform.position - knee.transform.position;

        hipKnee = knee.transform.position - hip.transform.position;
        spineBaseSpineMid = spineMid.transform.position - spineBase.transform.position;

        legAngle = Mathf.Rad2Deg * Mathf.Acos(Vector2.Dot(new Vector2(kneeHip.y, kneeHip.z).normalized, new Vector2(kneeAnkle.y, kneeAnkle.z).normalized));
        hipAngle = Mathf.Rad2Deg * Mathf.Acos(Vector2.Dot(new Vector2(spineBaseSpineMid.y, spineBaseSpineMid.z).normalized, new Vector2(hipKnee.y, hipKnee.z).normalized));

        if (legAngle <= 90) {
            hasHitDepth = true;
            depthMarker.GetComponent<Renderer>().material.color = new Color(0, 1, 0);
        }

        Debug.Log(hipAngle);

        if (hasHitDepth && legAngle >= 160 && hipAngle >= 160) {
            counter++;
            depthMarker.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
            hasHitDepth = false;
        }

        text.text = "" + counter;
    }
}
