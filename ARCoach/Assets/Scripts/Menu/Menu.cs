﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartRehab() {
        Application.LoadLevel("Rehab");
    }

    public void StartFitness() {
        Application.LoadLevel("Fitness");
    }
}
