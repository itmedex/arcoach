﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RepetitionRehab : MonoBehaviour {

    public GameObject spineShoulder;
    public GameObject spineMid;
    public GameObject shoulder;
    public GameObject elbow;

    public GameObject depthMarker;
    public Text text;

    private Vector3 spineShoulderSpineMid;
    private Vector3 shoulderElbow;

    private float shoulderVerticalAngle;
    private float delta;
    private float yInitialPosition;

    private bool hasHitHeight = false;
    private int counter = 0;

    // Use this for initialization
    void Start() {
        depthMarker.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
    }

    // Update is called once per frame
    void LateUpdate() {
        if (spineShoulder.transform.position == spineMid.transform.position)
            return;

        spineShoulderSpineMid = spineMid.transform.position - spineShoulder.transform.position;
        shoulderElbow = elbow.transform.position - shoulder.transform.position;

        shoulderVerticalAngle = Mathf.Rad2Deg * Mathf.Acos(Vector2.Dot(new Vector2(spineShoulderSpineMid.y, spineShoulderSpineMid.z).normalized, new Vector2(shoulderElbow.y, shoulderElbow.z).normalized));

        if (shoulderVerticalAngle >= 100) {
            hasHitHeight = true;
            depthMarker.GetComponent<Renderer>().material.color = new Color(0, 1, 0);
        }

        if (hasHitHeight && shoulderVerticalAngle <= 10) {
            counter++;
            depthMarker.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
            hasHitHeight = false;
        }

        text.text = "" + counter;
    }
}
